# README #

### What is this repository for? ###

* This is an application to view all the Stephen Benedict Podcasts by category, and to see where various films may be watched, whether on iTunes, Netflix or Amazon.
* version .1

### How do I get set up and run? ###

From the terminal:

* npm install

* http-server

### Who do I talk to? ###

* Developed by Duncan Mckenna.